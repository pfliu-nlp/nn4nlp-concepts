# Divergence in Machine Translation


## Semantic Divergence

#### how to characterize semantic divergences [paper](https://www.aclweb.org/anthology/D19-1532.pdf)
* word-level 



## Reference Divergence
How to compute the loss when the partial translation diverges from the references? (Explosure bias) [paper](https://www.aclweb.org/anthology/N19-1207.pdf)

* schedual sampling
* Align reference with Partil translation

## Style Divergence
#### Can we make MT sensitive to formality? [paper](https://www.aclweb.org/anthology/D19-1166.pdf)


## Future
How to rethink MT evaluation to respond to user needs?
How to define the differences between `style` and `semantic` invariance? (Morphology, style, culture, many dimensions of cultures)