# Story Generation

## Task Definition
Produce narrative text that from a given input prompt or context




## Challenges

* Many different plausible stories per prompt
* Do we need to model the planning process
* Stories can be really long
* A bunch of complex linguistic phenomenon
* Hard to evaluate


## Evaluation

### Automatical 
* match prompts with generated stories
* one prompt with multiple generated stories

### Human Evaluation
* `Fluency` 
* `Relevance` 
* `Coherence` 
* `Likability`



## Dataset

Storium: a dataset and evaluation platform for story generation (by Mohit Iyyer)
* stories are written by people
* structured supervision for planning

## Recommended Papers

* [Hierarchical Neural Story Generation](https://arxiv.org/pdf/1805.04833.pdf)

* [Plan-And-Write: Towards Better Automatic Storytelling](https://arxiv.org/pdf/1811.05701.pdf)

* [Controllable Neural Story Plot Generation via Reward Shaping](https://arxiv.org/pdf/1809.10736.pdf)

* [Do Massively Pretrained Language Models Make Better Storytellers?](https://www.aclweb.org/anthology/K19-1079.pdf)


## Other Stuff

### Step back (of Mohit Iyyer)
* Faster language models
* Characterizing bias in text 
* Large context LMs
* Narrative understanding
* Question generation
* Style transfer
* Coreference for literature

### Questions
* Do we really need Self-attention?
* It's not really clear what component is necessary?
* What if each head just indexed one position?

