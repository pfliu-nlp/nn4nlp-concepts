# Event Extraction


## Definition

### Basic Terminologies

* `Entity mention`: An entity is an object or set of objects in the world.
An entity mention is a reference to an entity in the form of a noun phrase or a pronoun. (credit to the [paper](https://www.cs.cmu.edu/~bishan/papers/joint_event_naacl16.pdf))
Entity mentions are with different types, such as *PER*, *ORG*, *GPE*, *LOC* and so forth.

* `Event trigger`: The word or phrase that clearly causes an event to fire.
Event triggers can be verbs, nouns, or adjectives. (i.e., *dead*, *bankrupt*)
Event trigger are with different types.

```
Note:
Event trigger could be a word or a text span, which is usually detected using sequence labeling method.
```

* `Event argument`: Event arguments are entities that fill specific roles in the event.
Event argment can be participants, general event attributes (i.e., place, time) and some specific attribute matters for the current event.
Event argument are with different semantic roles, such as: *crime*, *Defendant*.

### Definition of Event Extraction
Event extraction aims to identify the “trigger” words of specific types and their arguments.

### Relavant tasks
* Event relation extraction
* Event coreference resolution


## Dataset

* ACE 2005: It contains documents involving in different types of souces, such as newswire reports, weblogs. Detailed settings could refer to the [paper](https://www.cs.cmu.edu/~bishan/papers/joint_event_naacl16.pdf).

<body>
<table cellspacing="0" border="0">
	<colgroup span="7" width="80"></colgroup>
	<tr>
		<td height="19" align="left" valign=middle><font color="#000000"><br></font></td>
		<td align="left" valign=middle><font color="#000000"><br></font></td>
		<td align="left" valign=middle><font color="#000000">documents</font></td>
		<td align="left" valign=middle><font color="#000000">sentences</font></td>
		<td align="left" valign=middle><font color="#000000">triggers</font></td>
		<td align="left" valign=middle><font color="#000000">arguments</font></td>
		<td align="left" valign=middle><font color="#000000">ent-mentions</font></td>
	</tr>
	<tr>
		<td rowspan=3 height="56" align="center" valign=middle><font color="#000000">ACE2005</font></td>
		<td align="left" valign=middle><font color="#000000">train</font></td>
		<td align="right" valign=middle sdval="529" sdnum="1033;"><font color="#000000">529</font></td>
		<td align="right" valign=middle sdval="14837" sdnum="1033;"><font color="#000000">14837</font></td>
		<td align="right" valign=middle sdval="4337" sdnum="1033;"><font color="#000000">4337</font></td>
		<td align="right" valign=middle sdval="7768" sdnum="1033;"><font color="#000000">7768</font></td>
		<td align="right" valign=middle sdval="48797" sdnum="1033;"><font color="#000000">48797</font></td>
	</tr>
	<tr>
		<td align="left" valign=middle><font color="#000000">dev</font></td>
		<td align="right" valign=middle sdval="40" sdnum="1033;"><font color="#000000">40</font></td>
		<td align="right" valign=middle sdval="863" sdnum="1033;"><font color="#000000">863</font></td>
		<td align="right" valign=middle sdval="497" sdnum="1033;"><font color="#000000">497</font></td>
		<td align="right" valign=middle sdval="933" sdnum="1033;"><font color="#000000">933</font></td>
		<td align="right" valign=middle sdval="3917" sdnum="1033;"><font color="#000000">3917</font></td>
	</tr>
	<tr>
		<td align="left" valign=middle><font color="#000000">test</font></td>
		<td align="right" valign=middle sdval="30" sdnum="1033;"><font color="#000000">30</font></td>
		<td align="right" valign=middle sdval="672" sdnum="1033;"><font color="#000000">672</font></td>
		<td align="right" valign=middle sdval="438" sdnum="1033;"><font color="#000000">438</font></td>
		<td align="right" valign=middle sdval="911" sdnum="1033;"><font color="#000000">911</font></td>
		<td align="right" valign=middle sdval="4184" sdnum="1033;"><font color="#000000">4184</font></td>
	</tr>
</table>





## Recommended Papers
[Joint Extraction of Events and Entities within a Document Context](https://www.cs.cmu.edu/~bishan/papers/joint_event_naacl16.pdf) :star: :star:

[Joint Event Extraction via Recurrent Neural Networks](https://www.aclweb.org/anthology/N16-1034.pdf) :star: :star:

[Entity, Relation, and Event Extraction with Contextualized Span Representations](https://arxiv.org/pdf/1909.03546.pdf) :star: :star:



## Paperlist

* `Conferences & Journal`: ACL, EMNLP, NAACL, COLING, CONLL, CL, TACL 
* `Years`: 1979-2020
* [`System`](http://pfliu.com/pl-nlp/aclanthology-event-extraction.html) [Click Me!]
