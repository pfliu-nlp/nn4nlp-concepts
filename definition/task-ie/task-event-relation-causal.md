# Causal Relation

## Definition
Similar to temoporal relations, caucal relations can be represented by diected acyclic graphs, where the nodes are events and edges are tagged with either *before*, *after* (in temporal graphs), or *causes* and *caused by* (in causal grpahs). (credit to the [paper]())


## Method
Researchers focue on causal relation identification using lexcial features or discourse relations.


## Dataset
* [Causal-TimeBank dataset (Causal-TB)](https://www.aclweb.org/anthology/C14-1198.pdf): Causal-TB is sparse in temporal annotations and is even sparser in causal annotations.
* [EventCausality](https://www.aclweb.org/anthology/P14-2082.pdf)
* [MATRES](https://www.aclweb.org/anthology/P18-1212.pdf)

<table cellspacing="0" border="0">
	<colgroup span="5" width="80"></colgroup>
	<tr>
		<td height="19" align="left" valign=middle><font color="#000000"><br></font></td>
		<td align="left" valign=middle><font color="#000000">Doc</font></td>
		<td align="left" valign=middle><font color="#000000">Event</font></td>
		<td align="left" valign=middle><font color="#000000">Temporal-Rel</font></td>
		<td align="left" valign=middle><font color="#000000">Caucal-Rel</font></td>
	</tr>
	<tr>
		<td height="19" align="left" valign=middle><font color="#000000">TB-Dense</font></td>
		<td align="right" valign=middle sdval="36" sdnum="1033;"><font color="#000000">36</font></td>
		<td align="right" valign=middle sdval="1600" sdnum="1033;"><font color="#000000">1600</font></td>
		<td align="right" valign=middle sdval="5700" sdnum="1033;"><font color="#000000">5700</font></td>
		<td align="right" valign=middle><font color="#000000">-</font></td>
	</tr>
	<tr>
		<td height="19" align="left" valign=middle><font color="#000000">EventCausality</font></td>
		<td align="right" valign=middle sdval="25" sdnum="1033;"><font color="#000000">25</font></td>
		<td align="right" valign=middle sdval="800" sdnum="1033;"><font color="#000000">800</font></td>
		<td align="right" valign=middle><font color="#000000">-</font></td>
		<td align="right" valign=middle sdval="580" sdnum="1033;"><font color="#000000">580</font></td>
	</tr>
	<tr>
		<td height="19" align="left" valign=middle><font color="#000000">Causal-TB</font></td>
		<td align="right" valign=middle sdval="183" sdnum="1033;"><font color="#000000">183</font></td>
		<td align="right" valign=middle sdval="6800" sdnum="1033;"><font color="#000000">6800</font></td>
		<td align="right" valign=middle sdval="5100" sdnum="1033;"><font color="#000000">5100</font></td>
		<td align="right" valign=middle sdval="318" sdnum="1033;"><font color="#000000">318</font></td>
	</tr>
	<tr>
		<td height="19" align="left" valign=middle><font color="#000000">JointTC</font></td>
		<td align="right" valign=middle sdval="25" sdnum="1033;"><font color="#000000">25</font></td>
		<td align="right" valign=middle sdval="1300" sdnum="1033;"><font color="#000000">1300</font></td>
		<td align="right" valign=middle sdval="3400" sdnum="1033;"><font color="#000000">3400</font></td>
		<td align="right" valign=middle sdval="172" sdnum="1033;"><font color="#000000">172</font></td>
	</tr>
</table>



## Paperlist

* `Conferences & Journal`: ACL, EMNLP, NAACL, COLING, CONLL, CL, TACL 
* `Years`: 1979-2020
* [`System`](http://pfliu.com/pl-nlp/aclanthology-causal-relation.html) [Click Me!]
