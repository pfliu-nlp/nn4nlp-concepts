# Entity Linking

## Definition
Entity linking is also known as named entity disambiguation (NED).
Given a set of named entity mentions $`M={m_1,m_2,\cdots,m_k}`$ appeared in a document and a knowledge base $`KB`$ including a set of entities $`E = {e_1, e_2, \cdots, e_n}`$, an entity linking system aims to link these entity mentions to their referent entities in $`KB`$ ($`g: M \rightarrow E`$)

* Comparison with Named Entity Recognition
It differs from NER in that NER simply identifies the occurrence of a named entity in the text without identifying which specific entity it is.







## Paperlist

* `Conferences & Journal`: ACL, EMNLP, NAACL, COLING, CONLL, CL, TACL 
* `Years`: 1979-2020
* [`System`](http://pfliu.com/pl-nlp/aclanthology-entity-linking.html) [Click Me!]