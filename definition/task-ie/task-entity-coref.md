# Entity Coreference Resolution


##  Terminology

* `Entity mention`: An entity mention is a reference to an entity in the form of a noun phrase or a pronoun.

* `Antecedent`: An antecedent is an expression (word, phrase) that gives its meaning to a proform (pronoun,pro-verb) (credit to [Wikipedia](https://en.wikipedia.org/wiki/Antecedent_\(grammar\)))

* `Anaphor`: A word or phrase that refers to an earlier word or phrase.

* `Cluster`: A cluster is composed from entity mentions that are coferent.

```
Example: If you see *Peter*, give my key to *him*. (Antecedent-*Peter*, Anaphor-*him*)
```







## Methods

Given a document, the basic idea of entity coreference resolution follows two steps:
* #### Entity Mention Identifying
* #### Pair-wise Scoring


## Datasets

* CoNLL-2012: [This dataset](https://www.aclweb.org/anthology/W12-4501.pdf) contains 2802 training documents, 343 development documents, and 348 test documents. The training documents contain on average 454 words and a maximum of 4009 words.
* GAP: paragraph-level dataset constructed by the [paper](https://arxiv.org/pdf/1810.05201.pdf).



## Metrics
This [paper](https://www.aclweb.org/anthology/W10-4305.pdf) give a detailed description.
* MUC
* B$`^3`$
* CEAF$`_{{\phi}_4}`$


## Recommended Papers

[End-to-end Neural Coreference Resolution, EMNLP 2017](https://arxiv.org/pdf/1707.07045.pdf):star: :star: :star: :star:

[Higher-order Coreference Resolution with Coarse-to-fine Inference, NAACL 2018](https://www.aclweb.org/anthology/N18-2108.pdf) :star: :star:

[BERT for Coreference Resolution: Baselines and Analysis, Arixv.2019](https://arxiv.org/pdf/1908.09091.pdf) :star: :star:



## Paperlist

* `Conferences & Journal`: ACL, EMNLP, NAACL, COLING, CONLL, CL, TACL 
* `Years`: 1979-2020
* [`System`](http://pfliu.com/pl-nlp/aclanthology-entity-coref.html) [Click Me!]

