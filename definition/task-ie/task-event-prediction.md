# Event Sequence Prediction

## Definition
Event sequence prediction is a task to predict the next event1 based on a sequence of previously occurred events. 

#### Typology

##### Discrete / Continous
* Discrete-time event sequence prediction
* Continuous-time event sequence prediction

##### Time-dependent

* Time-independent: the actual time span between events is irrelevant and the distance between events is simply the difference between their order positions in the sequence
* Time-dependent: time spans are taken into consideration when we model a sequence

```
How to represent time in sequence prediction still largely remains under explored
```



## Method

#### Recurrent Neural Networks
* Time masking of event embedding 
* Event-time joint embedding



## Dataset
* `Electrical Medical Records`
* `Stack Overflow Dataset`
* `Financial Transaction Dataset`
* `App Usage Dataset`
* `Music Recommendation`


## Recommended Paper

* [Time-Dependent Representation for Neural Event Sequence Prediction](https://arxiv.org/abs/1708.00065) :star: :star: :star: :star:



## Paperlist

* `Conferences & Journal`: ACL, EMNLP, NAACL, COLING, CONLL, CL, TACL 
* `Years`: 1979-2020
* [`System`](http://pfliu.com/pl-nlp/aclanthology-event-prediction.html) [Click Me!]
