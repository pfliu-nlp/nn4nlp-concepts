# Event Sequencing

## Definition
Event sequencing task aims to group and order events from text documents belonging to the same script.
([Extract subsequence structure of events](http://cairo.lti.cs.cmu.edu/kbp/2016/after/))

#### `After Linking`
Input: a document with rich event mentions,
Output: `after-link` between events

#### `Parent-child Linking`
Input: a document with rich event mentions,
output: `parent-link` between events


<img src="fig/task/event-relation-seq.png" width="400">

credit to [this](http://cairo.lti.cs.cmu.edu/kbp/2016/after/)




## Recommended Papers

[Events Detection, Coreference and Sequencing: What’s next? Overview of the TAC KBP 2017 Event Track.](https://tac.nist.gov/publications/2017/additional.papers/TAC2017.KBP_Event_Nugget_overview.proceedings.pdf)
:star: :star:

[Joint Event and Temporal Relation Extraction with Shared Representations and Structured Prediction](https://arxiv.org/pdf/1909.05360.pdf)
:star: :star:

[Classifying Temporal Relations Between Events, ACL 2007](https://web.stanford.edu/~jurafsky/acl07-chambers.pdf)
:star: :star:


[Joint Event Trigger Identification and Event Coreference Resolution with
Structured Perceptron](https://www.aclweb.org/anthology/D15-1247.pdf)
:star: 

[Supervised Within-Document Event Coreference using Information Propagation](http://www.lrec-conf.org/proceedings/lrec2014/pdf/646_Paper.pdf)
:star: 

[Graph-Based Decoding for Event Sequencing and Coreference Resolution](https://arxiv.org/pdf/1806.05099.pdf)
:star: 


## Potential Ideas:
* How to incorporate the Antology Structural information into Event Relation?



## Paperlist

* `Conferences & Journal`: ACL, EMNLP, NAACL, COLING, CONLL, CL, TACL 
* `Years`: 1979-2020
* [`System`](http://pfliu.com/pl-nlp/aclanthology-causal-seq.html) [Click Me!]