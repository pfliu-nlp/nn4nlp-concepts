# Event Relation Extraction - Temporal Relation

## Definition
The task aims to extract temporal relatoins among events (i.e., *BEFORE*, *INCLUDES*, *VAGUE*), which can be formulated as building a graph for a given text, whose nodes represent events while edges are temporal relations.

##### Examples

<img src = "fig/task/event-relation.png" width="300">
(credit to this [paper](https://arxiv.org/pdf/1909.05360.pdf))

##### Structural Constrains
* `Event-Relation Consistency`
A Event relation holds if and only if both tokens are events.

* `Symmetry` 
The symmetry constraint forces two pairs of events with flipping orders to have re- versed relations.

* `Transitivity`
The transitivity constraints rules that if $`(i,j)`$, $`(k,j)`$
and $`(i,k)`$ pairs exist in the graph, the relation prediction of $`(i,k)`$
pair has to fall into the transitivity set specified by $`(i,j)`$, $`(k,j)`$ pairs.


## Methods
* Pipeline models
Decompose it as two separate tasks, event extraction and temporal relation classification.
```
Highlighting the drawbacks of pipeline models:
1) Inconsistent local decision
2) Error propagation

```

* Joint  models
Jointly extracts both events and temporal relations.

```
Basically, to extract event relations, researchers usually first conduct edge prediction locally, 
and then apply constraints globally by ILP algorithm (structure prediction).
```

## Datasets

* [TB-Dense](http://ucrel.lancs.ac.uk/publications/cl2003/papers/pustejovsky.pdf)

* [MATRES](https://www.aclweb.org/anthology/P18-1212.pdf)

* [CaTeRS](https://www.aclweb.org/anthology/W16-1007.pdf)

* [Event StoryLine](https://www.aclweb.org/anthology/W17-2711.pdf)

* [Tempeval-3](https://www.aclweb.org/anthology/S13-2001.pdf)

<table cellspacing="0" border="0">
	<colgroup span="4" width="80"></colgroup>
	<tr>
		<td height="19" align="left" valign=middle><font color="#000000"><br></font></td>
		<td align="left" valign=middle><font color="#000000"><br></font></td>
		<td align="left" valign=middle><font color="#000000">documents</font></td>
		<td align="left" valign=middle><font color="#000000">pairs</font></td>
	</tr>
	<tr>
		<td rowspan=3 height="56" align="center" valign=middle><font color="#000000">TB-Dense</font></td>
		<td align="left" valign=middle><font color="#000000">train</font></td>
		<td align="right" valign=middle sdval="22" sdnum="1033;"><font color="#000000">22</font></td>
		<td align="right" valign=middle sdval="4032" sdnum="1033;"><font color="#000000">4032</font></td>
	</tr>
	<tr>
		<td align="left" valign=middle><font color="#000000">dev</font></td>
		<td align="right" valign=middle sdval="5" sdnum="1033;"><font color="#000000">5</font></td>
		<td align="right" valign=middle sdval="629" sdnum="1033;"><font color="#000000">629</font></td>
	</tr>
	<tr>
		<td align="left" valign=middle><font color="#000000">test</font></td>
		<td align="right" valign=middle sdval="9" sdnum="1033;"><font color="#000000">9</font></td>
		<td align="right" valign=middle sdval="1427" sdnum="1033;"><font color="#000000">1427</font></td>
	</tr>
	<tr>
		<td rowspan=3 height="56" align="center" valign=middle><font color="#000000">MATRES</font></td>
		<td align="left" valign=middle><font color="#000000">train</font></td>
		<td align="right" valign=middle sdval="183" sdnum="1033;"><font color="#000000">183</font></td>
		<td align="right" valign=middle sdval="6332" sdnum="1033;"><font color="#000000">6332</font></td>
	</tr>
	<tr>
		<td align="left" valign=middle><font color="#000000">dev</font></td>
		<td align="right" valign=middle><font color="#000000">-</font></td>
		<td align="right" valign=middle><font color="#000000">-</font></td>
	</tr>
	<tr>
		<td align="left" valign=middle><font color="#000000">test</font></td>
		<td align="right" valign=middle sdval="20" sdnum="1033;"><font color="#000000">20</font></td>
		<td align="right" valign=middle sdval="827" sdnum="1033;"><font color="#000000">827</font></td>
	</tr>
</table>



## Paperlist

* `Conferences & Journal`: ACL, EMNLP, NAACL, COLING, CONLL, CL, TACL 
* `Years`: 1979-2020
* [`System`](http://pfliu.com/pl-nlp/aclanthology-temporal-relation.html) [Click Me!]

