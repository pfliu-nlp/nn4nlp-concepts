# Event Coreference Resolution

# Definition
Event coreference resolution aims to identify and link event mentions in a document that refer to the same real-world event, which is vital for identify- ing the skeleton of a story and text understanding and is beneficial to numerous other NLP applica- tions such as question answering and summarization


## Category

#### `Within- v.s. Cross-document`
* Within-document: determine whether two event mentions in the same documents denote the same event.
* Cross-document: predict if two event mentions in different documents refer to the same real-world event.

####  `End-to-end v.s. Non-end-to-end`
* end-to-end:   
* non-end-to-end: provides the gold event mentions


## Datasets

* MUC
* ACE
* OntoNotes
* ECB
* TAC-KBP (2015, 2016, 2017)
* [ProcessBank corpus](https://www.aclweb.org/anthology/D15-1247.pdf)


These datasets differ in several aspects. We could refer to this [paper](https://www.ijcai.org/Proceedings/2018/0773.pdf) to get the detail.

| Corpora   | E-typed/subtypes     | E-argument labeled? | WD/CD | n_docs |
|-----------|----------------------|---------------------|-------|--------|
| MUC       | Typed                | Yes                 | WD    | 60     |
| ACE       | 8 types, 33 subtypes | Yes                 | WD    | 600    |
| OntoNotes | untyped              | No                  | WD,CD | 600    |
| ECB       | untyped              | Yes                 | WD,CD | 1000   |
| KBP       | 9 types, 38 subtypes | Yes                 | WD    | 1000   |


## Methods
* Event mention identification and argument prediction
* Pair-wise event mention scoring
* Clustering based on pari-wise scoring function


## Evaluation Metrics
* Link-based $`MUC`$
* Mention-based $` B^3 `$
* Entity-based $`CEAF_e`$
* Rand index-based $`BLANC`$



## Typical Papers
* [Event Coreference Resolution: A Survey of Two Decades of Research](https://www.ijcai.org/Proceedings/2018/0773.pdf) :star: :star: :star: :star:

* [Graph-Based Decoding for Event Sequencing and Coreference Resolution](https://arxiv.org/pdf/1806.05099.pdf)
:star: :star:
* [Events Detection, Coreference and Sequencing: What’s next?](https://tac.nist.gov/publications/2017/additional.papers/TAC2017.KBP_Event_Nugget_overview.proceedings.pdf)
:star: :star:

* [Supervised Within-Document Event Coreference using Information Propagation](https://pdfs.semanticscholar.org/200d/cd81b19601831915f5b2f184587053933370.pdf) :star: :star:

* [Joint Event Trigger Identification and Event Coreference Resolution with Structured Perceptron](https://www.aclweb.org/anthology/D15-1247.pdf) :star: :star:




## Paperlist

* `Conferences & Journal`: ACL, EMNLP, NAACL, COLING, CONLL, CL, TACL 
* `Years`: 1979-2020
* [`System`](http://pfliu.com/pl-nlp/aclanthology-entity-coref.html) [Click Me!]