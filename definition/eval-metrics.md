# Evaluation Metrics




## `Recall` 

Given a binary classification task (with positive and negative classes),
we use the following confusion matrix to describe possible predictive behaviors.

|          | Positive (Ground truth)        | Negative (Ground truth)      |
|----------|-----------------|----------------|
| Positive (Predict) | True Positive ($`TP`$)   | False Positive ($`FP`$) |
| Negative (Predict)| False Negative($`FN`$) | True Negative  ($`TN`$)|


where
* `True positives`: samples predicted as positive that are actually positive.
* `False positives`: samples predicted as positive that are actually negative.
* `False negatives`: samples predicted as negative that are actually positive.
* `True negatives`: samples predicted as negative that are actually negative.

Recall ($`R`$) is the percentage of positive samples that are successfully predicted.

$`R = \frac{TP}{TP + FN}`$



## `Precision` 
Precision ($`P`$) is the percentage of predicted samples that are positive.

$`P = \frac{TP}{TP + FP}`$

## `F-measure` 
F-measure ($`F`$) is a measure that both takes Recall and Precision into account.

$`F_{\beta}= (1+\beta^{2}\cdot)\frac{P\cdot R}{\beta^2\cdot P + R} `$

Commonly, we specify $`\beta = 1`$ then we have the metric `F1-score`.
$`F1 = 2\cdot \frac{P\cdot R}{P+R}`$

## `Micro-/Macro- P, R, F`
In multi-class setting ($`k \in 1,2,\cdots,K`$), we still can use P (R,F) by averaging the value of each class.
Typically, there are two types of averaging methods:

### Micro averaging: 

$`MicroR = \frac{\sum_{k=1}^K {TP}^k}{\sum_{k=1}^K {{TP}^k + {FN}^k}}`$

$`MicroP = \frac{\sum_{k=1}^K {TP}^k}{\sum_{k=1}^K {{TP}^k + {FP}^k}}`$

$`MicroF1 = 2\cdot \frac{MicroP\cdot MicroR}{MicroP+MicroR}`$

### Micra averaging:

$`MacroR = \frac{1}{K}\sum_{k=1}^K R^k`$

$`MacroP = \frac{1}{K}\sum_{k=1}^K P^k`$

$`MacroF1 = 2\cdot \frac{MacroP\cdot MacroR}{MacroP+MacroR}`$

### Comparison
* Micro averaging is suitable for the dataset with unbalanced classes.



## `Rouge`


## `Bleu`


## `METEOR`


## `Perplexity`



## `Automatic Pyramid-based Evaluation`
<img src = "fig/task/eval-metric-pyramid.png" width="300">
(credit to the [paper](https://www.aclweb.org/anthology/D18-1450.pdf))

### General Pipeline

* Define the Summary Content Units (SCUs)
```
SCU is kind-of conceptual content and it could be instantiated as:
*Elementary Discourse Units (EDUs)* or *subject-predicate-object*.
```

* SCUs Identification

* Pyramid Construction:
 it's usually based on the number of reference summaries that contain the SCU.

* Automatic Scoring of Summaries: 
once we have constructed a pyramid, we could compute a score for a generated summary by calculating the alignment scoring between EDUs in the pyramid and EDUs in the generated summary.
