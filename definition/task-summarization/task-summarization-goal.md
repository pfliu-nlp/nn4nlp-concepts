# Goal-oriented Text Summarization



### Definition
It refers to the generated summaries that could satisfy the end-users’ intention or required information.

### Formulation
Goal-oriented text summarization system aims to generate a comparatively shorter text (S) given an input document (D) and goal (G):  

$`f(D,G) \rightarrow S`$

Let’s understand the following papers using the above definition:


<table cellspacing="0" border="0">
	<colgroup width="251"></colgroup>
	<colgroup width="116"></colgroup>
	<colgroup width="80"></colgroup>
	<colgroup width="92"></colgroup>
	<tr>
		<td style="border-top: 2px solid #000000; border-bottom: 2px solid #000000; border-left: 2px solid #000000; border-right: 2px solid #000000" height="49" align="center" valign=middle><b><i><font face="Comic Sans MS" color="#000000">papers</font></i></b></td>
		<td style="border-top: 2px solid #000000; border-bottom: 2px solid #000000; border-left: 2px solid #000000; border-right: 2px solid #000000" align="center" valign=middle><b><i><font face="Comic Sans MS" color="#000000">document (D)</font></i></b></td>
		<td style="border-top: 2px solid #000000; border-bottom: 2px solid #000000; border-left: 2px solid #000000; border-right: 2px solid #000000" align="center" valign=middle><b><i><font face="Comic Sans MS" color="#000000">goal (G)</font></i></b></td>
		<td style="border-top: 2px solid #000000; border-bottom: 2px solid #000000; border-left: 2px solid #000000; border-right: 2px solid #000000" align="center" valign=middle><b><i><font face="Comic Sans MS" color="#000000">Summary (S)</font></i></b></td>
	</tr>
	<tr>
		<td style="border-top: 2px solid #000000; border-bottom: 2px solid #000000; border-left: 2px solid #000000; border-right: 2px solid #000000" height="20" align="left" valign=middle><font face="Arial" color="#000000">Traditional Summarization</font></td>
		<td style="border-top: 2px solid #000000; border-bottom: 2px solid #000000; border-left: 2px solid #000000; border-right: 2px solid #000000" align="center" valign=middle><i><font face="Arial" color="#000000">document</font></i></td>
		<td style="border-top: 2px solid #000000; border-bottom: 2px solid #000000; border-left: 2px solid #000000; border-right: 2px solid #000000" align="center" valign=middle><font face="Arial" color="#000000">-</font></td>
		<td style="border-top: 2px solid #000000; border-bottom: 2px solid #000000; border-left: 2px solid #000000; border-right: 2px solid #000000" align="center" valign=middle><i><font face="Arial" color="#000000">summary</font></i></td>
	</tr>
	<tr>
		<td style="border-top: 2px solid #000000; border-bottom: 2px solid #000000; border-left: 2px solid #000000; border-right: 2px solid #000000" height="38" align="left" valign=middle><font face="Arial" color="#000000">Plan-And-Write: Towards Better Automatic Storytelling</font></td>
		<td style="border-top: 2px solid #000000; border-bottom: 2px solid #000000; border-left: 2px solid #000000; border-right: 2px solid #000000" align="center" valign=middle><font face="Arial" color="#000000">title</font></td>
		<td style="border-top: 2px solid #000000; border-bottom: 2px solid #000000; border-left: 2px solid #000000; border-right: 2px solid #000000" align="center" valign=middle><font face="Arial" color="#000000">storyline</font></td>
		<td style="border-top: 2px solid #000000; border-bottom: 2px solid #000000; border-left: 2px solid #000000; border-right: 2px solid #000000" align="center" valign=middle><font face="Arial" color="#000000">story</font></td>
	</tr>
	<tr>
		<td style="border-top: 2px solid #000000; border-bottom: 2px solid #000000; border-left: 2px solid #000000; border-right: 2px solid #000000" height="57" align="left" valign=middle><font face="Arial" color="#000000">Inducing Document Structure for Aspect-based Summarization</font></td>
		<td style="border-top: 2px solid #000000; border-bottom: 2px solid #000000; border-left: 2px solid #000000; border-right: 2px solid #000000" align="center" valign=middle><font face="Arial" color="#000000">document</font></td>
		<td style="border-top: 2px solid #000000; border-bottom: 2px solid #000000; border-left: 2px solid #000000; border-right: 2px solid #000000" align="center" valign=middle><font face="Arial" color="#000000">aspect</font></td>
		<td style="border-top: 2px solid #000000; border-bottom: 2px solid #000000; border-left: 2px solid #000000; border-right: 2px solid #000000" align="center" valign=middle><font face="Arial" color="#000000">summary</font></td>
	</tr>
	<tr>
		<td style="border-top: 2px solid #000000; border-bottom: 2px solid #000000; border-left: 2px solid #000000; border-right: 2px solid #000000" height="38" align="left" valign=middle><u><font face="Arial" color="#000000">Query-Chain Focused Summarization</font></u></td>
		<td style="border-top: 2px solid #000000; border-bottom: 2px solid #000000; border-left: 2px solid #000000; border-right: 2px solid #000000" align="center" valign=middle><font face="Arial" color="#000000">document</font></td>
		<td style="border-top: 2px solid #000000; border-bottom: 2px solid #000000; border-left: 2px solid #000000; border-right: 2px solid #000000" align="center" valign=middle><font face="Arial" color="#000000">query</font></td>
		<td style="border-top: 2px solid #000000; border-bottom: 2px solid #000000; border-left: 2px solid #000000; border-right: 2px solid #000000" align="center" valign=middle><font face="Arial" color="#000000">summary</font></td>
	</tr>
	<tr>
		<td style="border-top: 2px solid #000000; border-left: 2px solid #000000; border-right: 2px solid #000000" height="75" align="left" valign=middle><font face="Arial" color="#000000">Guiding Extractive Summarization with Question-Answering Rewards</font></td>
		<td style="border-top: 2px solid #000000; border-left: 2px solid #000000; border-right: 2px solid #000000" align="center" valign=middle><font face="Arial" color="#000000">document</font></td>
		<td style="border-top: 2px solid #000000; border-left: 2px solid #000000; border-right: 2px solid #000000" align="center" valign=middle><font face="Arial" color="#000000">(question, answer)</font></td>
		<td style="border-top: 2px solid #000000; border-left: 2px solid #000000; border-right: 2px solid #000000" align="center" valign=middle><font face="Arial" color="#000000">summary</font></td>
	</tr>
	<tr>
		<td style="border-top: 2px solid #000000; border-bottom: 2px solid #000000; border-left: 2px solid #000000; border-right: 2px solid #000000" height="57" align="left" valign=middle><font face="Arial" color="#000000">Retrieve, Rerank and Rewrite: Soft Template Based Neural Summarization</font></td>
		<td style="border-top: 2px solid #000000; border-bottom: 2px solid #000000; border-left: 2px solid #000000; border-right: 2px solid #000000" align="center" valign=middle><font face="Arial" color="#000000">document</font></td>
		<td style="border-top: 2px solid #000000; border-bottom: 2px solid #000000; border-left: 2px solid #000000; border-right: 2px solid #000000" align="center" valign=middle><font face="Arial" color="#000000">soft-template&nbsp;</font></td>
		<td style="border-top: 2px solid #000000; border-bottom: 2px solid #000000; border-left: 2px solid #000000; border-right: 2px solid #000000" align="center" valign=middle><font face="Arial" color="#000000">summary</font></td>
	</tr>
</table>

### General motivation of goal-oriented text generation

* Controllable
* Long generated text planning
* A framework to introduce the goal? (Just as Hovy has mentioned in Nanyun’s job talk, it’s rather challenging to let the text generator know our goal.)


### Key questions:

* How to define our “goal”?
* Probably we need to construct a new dataset or at least re-purpose a dataset?
* Besides “rouge”, we need some metrics that evaluate whether generated summaries fulfill the goal. 







One potential idea?
The goal could be defined as a set of sentences, topics, or keywords that users are interested in when they’re reading a long document.
Therefore, we shift the form of traditional text summarization from f(D) -> S
to f(D,G) -> S. It could be:
document, goal_sentences -> summary
document, goal_topics → summary
document, goal_keywords → summary

Q: How can we obtain the goal_sentences, goal_topics, goal_keywords?
A: It should be collected based on the gold summary of a given document. For example, we should verify that goal_topics can be covered by the goal summary.

Q: Should we make extra annotations? 
A: Here, one interesting thing is that probably we could directly learn a goal-oriented summarizer using existing datasets. 

For example,
Supposing a document D_i covers five topics {people, events, number, environment, emotion}, and its corresponding goal summary S_i covers two of them {people, number}.
We could force our model to learn: how to generate a summary when our input goal is “people” or “number.” And hopefully, the model could generalize to other goals.
In a sense, we even could learn a big goal-space by multi-domain jointly training since different domains potentially could contribute goals diversely.
