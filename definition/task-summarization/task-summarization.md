# Text Summarization





## Evaluation Methodology




### Intrinsic v.s. Extrinsic

`Intrinsic` metrics measure a summary’s quality by measuring its similarity
to a manually produced target gold summary or by inspecting properties of the summary

`Extrinsic` metrics test the ability of a summary to support performing related tasks and compare the performance of humans or systems when completing a
task that requires understanding the source document


### Model-based v.s. Model-free

`Model-based`: Relying on an external learned model.

`Model-free`



### Examples

* [Pyramid](http://www.cs.columbia.edu/~ani/papers/pyramid.pdf)

* [Basic Elements](https://pdfs.semanticscholar.org/45fc/709a2fb8cd3cc71462c65e3d5e1bcb23c444.pdf)

* [ROUGE](https://www.aclweb.org/anthology/W04-1013.pdf) 

* [Responsiveness](https://pdfs.semanticscholar.org/dc95/4000617ae982f83b7f1ed4bd72b95e38aa88.pdf)

* [Grammaticality](https://pdfs.semanticscholar.org/5e61/47a265bfd90eb21386a3b9430d6cd6097ad0.pdf)

* [Readability](https://pdfs.semanticscholar.org/5e61/47a265bfd90eb21386a3b9430d6cd6097ad0.pdf)

* [Non-redundancy](https://pdfs.semanticscholar.org/5e61/47a265bfd90eb21386a3b9430d6cd6097ad0.pdf)

* [Coherency](https://pdfs.semanticscholar.org/5e61/47a265bfd90eb21386a3b9430d6cd6097ad0.pdf)

* [Abstractiveness](https://www.aclweb.org/anthology/D18-1089.pdf)

* [Generalization](XXX)

* [ROUGE-WordEmb](https://www.aclweb.org/anthology/D15-1222.pdf)

* [BERTScore](https://arxiv.org/pdf/1904.09675.pdf)

* [APES](https://www.aclweb.org/anthology/N19-1395.pdf)

* [Fact-triplet](https://arxiv.org/pdf/1905.13322.pdf)

* [FactCC](https://arxiv.org/pdf/1910.12840.pdf)



<table cellspacing="0" border="0">
	<colgroup span="5" width="80"></colgroup>
	<tr>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000" height="19" align="left" valign=middle><b><font color="#000000">Metrics</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000" align="left" valign=middle><b><font color="#000000">Intrinsic</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000" align="left" valign=middle><b><font color="#000000">Extrinsic</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000" align="left" valign=middle><b><font color="#000000">Model-free</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000" align="left" valign=middle><b><font color="#000000">Model-based</font></b></td>
	</tr>
	<tr>
		<td height="19" align="left" valign=middle><font color="#000000">Pyramid</font></td>
		<td align="center" valign=middle><font color="#000000">&radic;</font></td>
		<td align="center" valign=middle><font color="#000000"><br></font></td>
		<td align="center" valign=middle><font color="#000000">&radic;</font></td>
		<td align="center" valign=middle><font color="#000000"><br></font></td>
	</tr>
	<tr>
		<td height="19" align="left" valign=middle><font color="#000000">Basic Elements</font></td>
		<td align="center" valign=middle><font color="#000000">&radic;</font></td>
		<td align="center" valign=middle><font color="#000000"><br></font></td>
		<td align="center" valign=middle><font color="#000000">&radic;</font></td>
		<td align="center" valign=middle><font color="#000000"><br></font></td>
	</tr>
	<tr>
		<td height="19" align="left" valign=middle><font color="#000000">ROUGE</font></td>
		<td align="center" valign=middle><font color="#000000">&radic;</font></td>
		<td align="center" valign=middle><font color="#000000"><br></font></td>
		<td align="center" valign=middle><font color="#000000">&radic;</font></td>
		<td align="center" valign=middle><font color="#000000"><br></font></td>
	</tr>
	<tr>
		<td height="19" align="left" valign=middle><font color="#000000">Responsiveness</font></td>
		<td align="center" valign=middle><font color="#000000">&radic;</font></td>
		<td align="center" valign=middle><font color="#000000"><br></font></td>
		<td align="center" valign=middle><font color="#000000">&radic;</font></td>
		<td align="center" valign=middle><font color="#000000"><br></font></td>
	</tr>
	<tr>
		<td height="19" align="left" valign=middle><font color="#000000">Grammaticality</font></td>
		<td align="center" valign=middle><font color="#000000">&radic;</font></td>
		<td align="center" valign=middle><font color="#000000"><br></font></td>
		<td align="center" valign=middle><font color="#000000">&radic;</font></td>
		<td align="center" valign=middle><font color="#000000"><br></font></td>
	</tr>
	<tr>
		<td height="19" align="left" valign=middle><font color="#000000">Readability</font></td>
		<td align="center" valign=middle><font color="#000000">&radic;</font></td>
		<td align="center" valign=middle><font color="#000000"><br></font></td>
		<td align="center" valign=middle><font color="#000000">&radic;</font></td>
		<td align="center" valign=middle><font color="#000000"><br></font></td>
	</tr>
	<tr>
		<td height="19" align="left" valign=middle><font color="#000000">Non-redundancy</font></td>
		<td align="center" valign=middle><font color="#000000">&radic;</font></td>
		<td align="center" valign=middle><font color="#000000"><br></font></td>
		<td align="center" valign=middle><font color="#000000">&radic;</font></td>
		<td align="center" valign=middle><font color="#000000"><br></font></td>
	</tr>
	<tr>
		<td height="19" align="left" valign=middle><font color="#000000">Coherency</font></td>
		<td align="center" valign=middle><font color="#000000">&radic;</font></td>
		<td align="center" valign=middle><font color="#000000"><br></font></td>
		<td align="center" valign=middle><font color="#000000">&radic;</font></td>
		<td align="center" valign=middle><font color="#000000">&radic;</font></td>
	</tr>
	<tr>
		<td height="19" align="left" valign=middle><font color="#000000">Abstractiveness</font></td>
		<td align="center" valign=middle><font color="#000000">&radic;</font></td>
		<td align="center" valign=middle><font color="#000000"><br></font></td>
		<td align="center" valign=middle><font color="#000000">&radic;</font></td>
		<td align="center" valign=middle><font color="#000000"><br></font></td>
	</tr>
	<tr>
		<td height="19" align="left" valign=middle><font color="#000000">Generalization</font></td>
		<td align="center" valign=middle><font color="#000000">&radic;</font></td>
		<td align="center" valign=middle><font color="#000000"><br></font></td>
		<td align="center" valign=middle><font color="#000000">&radic;</font></td>
		<td align="center" valign=middle><font color="#000000"><br></font></td>
	</tr>
	<tr>
		<td height="19" align="left" valign=middle><font color="#000000">ROUGE-WordEmb</font></td>
		<td align="center" valign=middle><font color="#000000">&radic;</font></td>
		<td align="center" valign=middle><font color="#000000"><br></font></td>
		<td align="center" valign=middle><font color="#000000"><br></font></td>
		<td align="center" valign=middle><font color="#000000">&radic;</font></td>
	</tr>
	<tr>
		<td height="19" align="left" valign=middle><font color="#000000">BERTScore</font></td>
		<td align="center" valign=middle><font color="#000000">&radic;</font></td>
		<td align="center" valign=middle><font color="#000000"><br></font></td>
		<td align="center" valign=middle><font color="#000000"><br></font></td>
		<td align="center" valign=middle><font color="#000000">&radic;</font></td>
	</tr>
	<tr>
		<td height="19" align="left" valign=middle><font color="#000000">APES</font></td>
		<td align="center" valign=middle><font color="#000000"><br></font></td>
		<td align="center" valign=middle><font color="#000000">&radic;</font></td>
		<td align="center" valign=middle><font color="#000000"><br></font></td>
		<td align="center" valign=middle><font color="#000000">&radic;</font></td>
	</tr>
	<tr>
		<td height="19" align="left" valign=middle><font color="#000000">Fact-triplet</font></td>
		<td align="center" valign=middle><font color="#000000">&radic;</font></td>
		<td align="center" valign=middle><font color="#000000"><br></font></td>
		<td align="center" valign=middle><font color="#000000"><br></font></td>
		<td align="center" valign=middle><font color="#000000">&radic;</font></td>
	</tr>
	<tr>
		<td style="border-bottom: 1px solid #000000" height="19" align="left" valign=middle><font color="#000000">FactCC</font></td>
		<td style="border-bottom: 1px solid #000000" align="center" valign=middle><font color="#000000">&radic;</font></td>
		<td style="border-bottom: 1px solid #000000" align="center" valign=middle><font color="#000000"><br></font></td>
		<td style="border-bottom: 1px solid #000000" align="center" valign=middle><font color="#000000"><br></font></td>
		<td style="border-bottom: 1px solid #000000" align="center" valign=middle><font color="#000000">&radic;</font></td>
	</tr>
</table>


### Semantic Unit
The definition of semantic unit is diverse in previous work.

* [token](https://www.aclweb.org/anthology/P19-1101.pdf)

* [entity](https://arxiv.org/pdf/1910.14142.pdf) 

* [elementary discourse unit (EDU)](http://www.cs.columbia.edu/~ani/papers/pyramid.pdf)

* [sentence]()

* triplet
	- [subject-relation-object](https://arxiv.org/pdf/1905.13322.pdf)
	- [subject-predicate-object]()



* [topic]()


### Design Philosophy of Evaluation Metrics

<img src = "fig/task/eval/eval-trilogy.png" width="300">






