# Parameter Tuning Tricks for Newbie

* Have you used the pre-trained word embeddings?
* Have you shuffled your datasets?
* Have you used the dropout?
* Is you hidden size too large or too small?