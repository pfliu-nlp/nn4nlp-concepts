# Pretrained Encoders and Representation

## Word-level Methods

### Non-contextualized
Each word is represented as a low-dimensional vector with real value. And this vector wouldn't change even if its context words are changed.


* `word2vec`: [Distributed Representations of Words and Phrases and their Compositionality](http://papers.nips.ccpaper/5021-distributed-representations-of-words-and-phrases-and-their-compositionality.pdf)
* `word2vec`: [Efficient Estimation of Word Representations in Vector Space](https://arxiv.org/pdf/1301.3781.pdf)
* `GLoVe`:[GloVe: Global Vectors for Word Representation
](https://nlp.stanford.edu/pubs/glove.pdf)


### Contextualized
Instead of using a fixed vector,  we could represent a word whose vector would change based on different context information (surrounded words).


* `SALSTM`: [Semi-supervised Sequence Learning](http://papers.nips.cc/paper/5949-semi-supervised-sequence-learning.pdf)
* `CoVe`: [Learned in Translation: Contextualized Word Vectors](http://papers.nips.cc/paper/7209-learned-in-translation-contextualized-word-vectors.pdf)
* `ELMo`: [Deep contextualized word representations](https://arxiv.org/pdf/1802.05365.pdf)
* `GPT1.0`: [Improving Language Understanding
by Generative Pre-Training](https://s3-us-west-2.amazonaws.com/openai-assets/research-covers/language-unsupervised/language_understanding_paper.pdf)
* `BERT`: [BERT: Pre-training of Deep Bidirectional Transformers for
Language Understanding](https://arxiv.org/pdf/1810.04805.pdf)
* `GPT2.0`: [Language Models are Unsupervised Multitask Learners](https://d4mucfpksywv.cloudfront.net/better-language-models/language_models_are_unsupervised_multitask_learners.pdf)
* `MASS`: [MASS: Masked Sequence to Sequence Pre-training for Language Generation](https://arxiv.org/pdf/1905.02450.pdf)
* `UNILM` [Unified Language Model Pre-training for
Natural Language Understanding and Generation](http://papers.nips.cc/paper/9464-unified-language-model-pre-training-for-natural-language-understanding-and-generation.pdf)
* `XLNet`: [XLNet: Generalized Autoregressive Pretraining
for Language Understanding](http://papers.nips.cc/paper/8812-xlnet-generalized-autoregressive-pretraining-for-language-understanding.pdf)
* `SpanBERT`: [SpanBERT: Improving Pre-training by Representing
and Predicting Spans](https://arxiv.org/pdf/1907.10529.pdf)
* `ALBERT`: [ALBERT: A Lite BERT for Self-supervised Learning of Language Representations](https://arxiv.org/pdf/1909.11942.pdf)
* `RoBERTa`: [RoBERTa: A Robustly Optimized BERT Pretraining Approach](https://arxiv.org/pdf/1907.11692.pdf)
* `ELECTRA`: [ELECTRA: Pre-training Text Encoders as Discriminators Rather Than Generators](https://openreview.net/pdf?id=r1xMH1BtvB)


## Sentence-level Methods

`ParaVector`: [Distributed Representations of Sentences and Documents](https://cs.stanford.edu/~quocle/paragraph_vector.pdf)

`Skip-thought`: [Skip-Thought Vectors](https://arxiv.org/pdf/1506.06726.pdf)
