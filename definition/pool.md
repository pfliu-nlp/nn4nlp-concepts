# Pooling
Pooling is an aggregation operation. Given an input vector (matrix or tensor),
it aims to select specific entries based on certain pre-defined criteria.
Typically, we introduce four different types of pooling operations and exemplify them with a one-dimensional vector.

## Max-pooling
Given a vector $`[0,-1,8,6,1,-2]`$, it will choose one maximum value $`8`$.

<img src = "fig/pool/max.png" width="300">


## Mean-pooling
Given a vector $`[0,-1,8,6,1,-2]`$, it will output the average of the vector: $`\frac{0-1+8+6+1-2}{6}`$ and the result is $`2`$.

<img src = "fig/pool/mean.png" width="300">

## K-max pooling 
Given a vector $`[0,-1,8,6,1,-2]`$, it will choose  Top-K (here $`k=2`$) maximum values $`8, 6`$.

<img src = "fig/pool/kmax.png" width="320">

## K-dynamic pooling
Given a vector $`[0,-1,8,6,1,-2]`$, it will first evenly partition the vector into K (here $`K=3`$ ) groups. And the maximum value in each group will be selected: $`0, 8, 1`$

<img src = "fig/pool/dynamic.gif" width="380">