# Word Representation



## Teminology

#### `Distributal Representation`
*Distributional* can be understood as distributional properties in a large text corpus.
One typical application is *distributioal hypothesis*, which says "linguistic units (i.e., words, phrases) with similar distributions have similar meanings." (credit to [wiki](https://en.wikipedia.org/wiki/Distributional_semantics)
, suggesting that a word is characterized by the company it keeps (credit to [John Rupert Firth](https://en.wikipedia.org/wiki/John_Rupert_Firth)).

#### `Distributed Representation`
*Distributed* is used to characterize each linguistic unit using a low-dimensinal space.
One common application is *distributed word representations*, in which each word is represented as a low-dimensional vector with real values.
For example, the word *apple* could be represented as $`[0.2, 0.3, 0.1, -0.5, 0.5] \in \mathrm{R}`$
```
 Given a distributed word representation, each dimension is not explanable while the whole vector could represent some concepts.
```

#### `Symbolic Representation`
*Symbolic representation* aims to represent a signal with a set of discrete concpets, which usually are interpretable.


Typical papers:
[Distributed Representations](https://web.stanford.edu/~jlmcc/papers/PDP/Chapter3.pdf)
[Distributed Representations](http://www.cs.toronto.edu/~bonner/courses/2014s/csc321/lectures/lec5.pdf)





### Big Event for Word Embeddings

<img src = "fig/task/task-wordemb.png" width="700">

* [Neural Language Model](http://www.jmlr.org/papers/volume3/bengio03a/bengio03a.pdf)
* [Hierarchical Distributed Language Model](https://www.cs.toronto.edu/~amnih/papers/hlbl_final.pdf)
* [C&W Embedding](https://ronan.collobert.com/pub/matos/2008_nlp_icml.pdf)
* [](http://www.cs.cmu.edu/~bhiksha/courses/deeplearning/Fall.2016/pdfs/1106/Word_Representations.pdf)
* []()
