# Convolutional Neural Networks (arch-cnn)



## Definition in Sigal Processing
Mathematically, convolution operation involves two versions: ``continuous`` and ``discrete``, which differ in the types of an input signal.

#### Continous
It is defined as the integral of two functions' products after one function has been reversed and shifted.
For example, the convolution of function $`f`$ and $`g`$ could be defined as:

$`   (f*g)(t) = \int_{-\infty}^{\infty}f(t-\tau)g(\tau) d\tau `$

#### Discrete
It is defined as the summation of two sequences' product after one is reversed and shifted.

$` (f*g)(n) = \sum_{m=-m}^M f[n-m]g[m] `$



## Definition in Neural Networks
When we bring convolution operation into neural networks, the reversed operation is not necessary, therefore obtaining the following convolution equation:

$` (f*g)(n) = \sum_{m=-m}^M f[n]g[m] `$



### Basic Concepts in CNNs
In what follows, I will detail some fundamental concepts in convolutional neural networks.

#### `One/Two- dimensional Convolution`
Convolution can happen with different forms (1d,2d,3d, and so forth). The critical difference rests in the dimensionality of the input signal and how the filters slide cross input. 

* ##### One-dimensional

One-dimensional (1d) convolution is defined as the summation of two sequences' products.
Here is an example of the 1d convolution operation.
Specifically, the blue, red, and gree boxes represent input vector, filter, and output, respectively.

<img src="fig/arc-cnn/1d-narrow.gif" width="150">

* ##### Two-dimensional

Two-dimensional (2d) convolution is defined as the summation of two matrices' products.
The following figure shows this case.

<img src="fig/arc-cnn/2d.gif" width="300">


#### `Stride`
Stride represents the number of units shifts over the input signal (i.e., vector, matrix.)
For example, the stride size of the above figure equals one while the following chart shows the case stride equals two.

<img src="fig/arc-cnn/2d-2stride.gif" width="300">







#### `Padding`
Padding allows us to deal with the units at the boundary of the input signal.
Typically, according to different padding ways, we can understand convolutional operation from three views.

* ###### Narrow Convolution
<img src="fig/arc-cnn/1d-narrow.gif" width="150">

* ###### Equal (Valid) Convolution
<img src="fig/arc-cnn/1d-equal.gif" width="170">

* ###### wide Convolution
<img src="fig/arc-cnn/1d-wide.gif" width="210">


#### `Multiple Filters`
We can introduce multiple filters and each one represents a unique feature of the convolution window.
The following figure show the case: filters = 2

<img src="fig/arc-cnn/1d-2filters.gif" width="250">



### Application

#### A Case Study
We take the sentiment classification as an interpretable example and demonstrate how we apply the CNNs in a real-world task (credit to this [paper](https://arxiv.org/pdf/1408.5882.pdf)).
The following figure illustrates the overview learning framework, which consists of a *lookup table*, *embedding layer (input)*, *convolutional layer*, *pooling layer* and *output layer*.

<img src="fig/arc-cnn/case-overview.png" width="600">

Let's see how convolutional and pooling layers have operated.

<img src="fig/arc-cnn/case-cnn.gif" width="250">



### Recommended Papers


[Convolutional Neural Networks for Sentence Classification](https://arxiv.org/pdf/1408.5882.pdf) :star: :star: :star: :star: :star:

[A Convolutional Neural Network for Modelling Sentences](https://arxiv.org/pdf/1404.2188.pdf) :star: :star: :star: :star: